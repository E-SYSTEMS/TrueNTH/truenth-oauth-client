class ProjectVersion {

    Integer major
    Integer minor
    Integer patch

    Boolean snap

    ProjectVersion(Integer major, Integer minor, Integer patch) {
        this.major = major
        this.minor = minor
        this.patch = patch
        this.snap = true
    }

    ProjectVersion(Integer major, Integer minor, Integer patch, Boolean snap) {
        this.major = major
        this.minor = minor
        this.patch = patch
        this.snap = snap
    }

    @Override
    String toString() {
        getVertion
    }

    String getBaseVersion() {
        "$major.$minor.$patch"
    }

    String getVersion() {
        if (snap) {
            String buildDate = new Date().format('yyyyMMddHHmmss');
            "$major.$minor.$patch-" + buildDate + "-SNAPSHOT";
        } else
            "$major.$minor.$patch"
    }

    String getQualifiedVersionSymbol() {
        String qualifier = (snap) ? "-SNAPSHOT" : "";
        getBaseVersion() + qualifier
    }
}
