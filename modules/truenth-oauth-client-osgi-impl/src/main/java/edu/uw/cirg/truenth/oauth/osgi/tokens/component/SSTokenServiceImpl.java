package edu.uw.cirg.truenth.oauth.osgi.tokens.component;

import edu.uw.cirg.truenth.oauth.osgi.tokens.SSTokenService;
import edu.uw.cirg.truenth.oauth.tokens.TrueNTHAccessToken;
import edu.uw.cirg.truenth.oauth.tokens.TrueNTHRefreshToken;
import edu.uw.cirg.truenth.oauth.tokens.factory.TrueNTHAccessTokenFactory;
import org.osgi.service.component.annotations.Component;

import java.time.Instant;

@Component( immediate = true,
			service = SSTokenService.class )
public class SSTokenServiceImpl implements SSTokenService {

	@Override
	public TrueNTHAccessToken getTrueNTHAccessToken( String accessToken, String refreshToken, Instant date, Integer expiresIn, String scope ) {

		return TrueNTHAccessTokenFactory.getTrueNTHAccessToken( accessToken, refreshToken, date, expiresIn, scope );
	}

	@Override
	public TrueNTHRefreshToken getTrueNTHRefreshToken( String value ) {

		return TrueNTHAccessTokenFactory.getTrueNTHRefreshToken( value );
	}
}
