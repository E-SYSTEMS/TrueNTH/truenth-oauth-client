package edu.uw.cirg.truenth.oauth.osgi.signedrequest.component;

import edu.uw.cirg.truenth.oauth.osgi.signedrequest.SSSignedRequestService;
import edu.uw.cirg.truenth.oauth.signedrequest.TrueNTHSignedRequest;
import edu.uw.cirg.truenth.oauth.signedrequest.factory.TrueNTHSignedRequestFactory;
import org.osgi.service.component.annotations.Component;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

@Component( immediate = true,
			service = SSSignedRequestService.class )
public class SSSignedRequestServiceImpl implements SSSignedRequestService {

	@Override
	public TrueNTHSignedRequest getTrueNTHSignedRequest( String signedRequest, String validationKey ) throws InvalidKeyException, NoSuchAlgorithmException {

		return TrueNTHSignedRequestFactory.getTrueNTHSignedRequest( signedRequest, validationKey );
	}

}
