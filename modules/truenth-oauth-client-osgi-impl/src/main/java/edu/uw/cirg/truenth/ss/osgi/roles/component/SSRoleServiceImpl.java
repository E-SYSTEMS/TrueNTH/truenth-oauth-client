package edu.uw.cirg.truenth.ss.osgi.roles.component;

import edu.uw.cirg.truenth.ss.osgi.roles.SSRoleService;
import edu.uw.cirg.truenth.ss.roles.SSRole;
import edu.uw.cirg.truenth.ss.roles.SSRoleExtractor;
import edu.uw.cirg.truenth.ss.roles.factory.SSRoleExtractorFactory;
import edu.uw.cirg.truenth.ss.roles.factory.SSRoleFactory;
import org.osgi.service.component.annotations.Component;

import javax.json.JsonObject;
import java.util.List;

@Component( immediate = true,
			service = SSRoleService.class )
public class SSRoleServiceImpl implements SSRoleService {

	private final SSRoleExtractor< JsonObject > extractor = SSRoleExtractorFactory.getSSRoleExtractorJson( );

	@Override
	public SSRole getSSRole( ) {

		return SSRoleFactory.getSSRole( );
	}

	@Override
	public List< SSRole > extractRoles( JsonObject data ) {

		return extractor.extractRoles( data );
	}
}
