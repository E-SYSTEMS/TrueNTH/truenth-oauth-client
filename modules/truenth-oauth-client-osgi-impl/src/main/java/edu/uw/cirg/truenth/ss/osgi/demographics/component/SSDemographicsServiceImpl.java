package edu.uw.cirg.truenth.ss.osgi.demographics.component;

import edu.uw.cirg.truenth.ss.demographics.SSDemographics;
import edu.uw.cirg.truenth.ss.demographics.SSDemographicsExtractor;
import edu.uw.cirg.truenth.ss.demographics.factory.SSDemographicsExtractorFactory;
import edu.uw.cirg.truenth.ss.demographics.factory.SSDemographicsFactory;
import edu.uw.cirg.truenth.ss.osgi.demographics.SSDemographicsService;
import org.osgi.service.component.annotations.Component;

import javax.json.JsonObject;

@Component( immediate = true,
			service = SSDemographicsService.class )
public class SSDemographicsServiceImpl implements SSDemographicsService {

	private final SSDemographicsExtractor< JsonObject > extractor = SSDemographicsExtractorFactory.getSSDemographicsExtractorJson( );

	@Override
	public SSDemographics getSSDemographics( ) {

		return SSDemographicsFactory.getSSDemographics( );
	}

	@Override
	public SSDemographics extractDemographics( JsonObject data ) {

		return extractor.extractDemographics( data );
	}
}
