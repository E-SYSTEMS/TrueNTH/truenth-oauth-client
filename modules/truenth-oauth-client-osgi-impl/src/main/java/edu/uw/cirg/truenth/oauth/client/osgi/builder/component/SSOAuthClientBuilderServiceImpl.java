package edu.uw.cirg.truenth.oauth.client.osgi.builder.component;

import edu.uw.cirg.truenth.oauth.client.builder.TrueNTHOAuthClientBuilder;
import edu.uw.cirg.truenth.oauth.client.builder.factory.TrueNTHOAuthClientBuilderFactory;
import edu.uw.cirg.truenth.oauth.client.osgi.builder.SSOAuthClientBuilderService;
import org.osgi.service.component.annotations.Component;

@Component( immediate = true,
			service = SSOAuthClientBuilderService.class )
public class SSOAuthClientBuilderServiceImpl implements SSOAuthClientBuilderService {

	@Override
	public TrueNTHOAuthClientBuilder getTrueNTHOAuthClientBuilder( ) {

		return TrueNTHOAuthClientBuilderFactory.getTrueNTHOAuthClientBuilder( );
	}
}
