package edu.uw.cirg.truenth.oauth.osgi.tokens;

import edu.uw.cirg.truenth.oauth.tokens.TrueNTHAccessToken;
import edu.uw.cirg.truenth.oauth.tokens.TrueNTHRefreshToken;
import org.osgi.annotation.versioning.ProviderType;

import java.time.Instant;

@ProviderType
public interface SSTokenService {

	TrueNTHAccessToken getTrueNTHAccessToken( String accessToken, String refreshToken, Instant date, Integer expiresIn, String scope );

	TrueNTHRefreshToken getTrueNTHRefreshToken( String value );
}
