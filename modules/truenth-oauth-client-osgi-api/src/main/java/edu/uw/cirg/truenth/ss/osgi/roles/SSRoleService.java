package edu.uw.cirg.truenth.ss.osgi.roles;

import edu.uw.cirg.truenth.ss.roles.SSRole;
import org.osgi.annotation.versioning.ProviderType;

import javax.json.JsonObject;
import java.util.List;

@ProviderType
public interface SSRoleService {

	SSRole getSSRole( );
	List< SSRole > extractRoles( JsonObject data );
}
