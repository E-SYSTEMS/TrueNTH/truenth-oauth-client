package edu.uw.cirg.truenth.oauth.client.osgi.builder;

import edu.uw.cirg.truenth.oauth.client.builder.TrueNTHOAuthClientBuilder;
import org.osgi.annotation.versioning.ProviderType;

@ProviderType
public interface SSOAuthClientBuilderService {

	TrueNTHOAuthClientBuilder getTrueNTHOAuthClientBuilder( );
}
