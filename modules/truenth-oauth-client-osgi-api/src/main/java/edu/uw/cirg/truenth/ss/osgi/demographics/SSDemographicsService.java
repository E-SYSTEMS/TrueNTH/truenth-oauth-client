package edu.uw.cirg.truenth.ss.osgi.demographics;

import edu.uw.cirg.truenth.ss.demographics.SSDemographics;
import org.osgi.annotation.versioning.ProviderType;

import javax.json.JsonObject;

@ProviderType
public interface SSDemographicsService {

	SSDemographics getSSDemographics( );
	SSDemographics extractDemographics( JsonObject data );
}
