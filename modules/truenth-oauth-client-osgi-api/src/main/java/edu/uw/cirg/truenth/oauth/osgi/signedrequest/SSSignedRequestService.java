package edu.uw.cirg.truenth.oauth.osgi.signedrequest;

import edu.uw.cirg.truenth.oauth.signedrequest.TrueNTHSignedRequest;
import org.osgi.annotation.versioning.ProviderType;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

@ProviderType
public interface SSSignedRequestService {

	TrueNTHSignedRequest getTrueNTHSignedRequest( String signedRequest, String validationKey ) throws InvalidKeyException, NoSuchAlgorithmException;

}
