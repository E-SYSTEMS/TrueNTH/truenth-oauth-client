package edu.uw.cirg.truenth.ss.internal.demographics;

import edu.uw.cirg.truenth.ss.demographics.SSDemographics;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;

public class SSDemographicsImpl implements SSDemographics {

	private Calendar birthday;
	private String email;
	private String firstName;
	private String lastName;
	private boolean gender;
	private URL photoUrl;
	private String trueNTHUsername;
	private long trueNTHID;

	@Override
	public int hashCode( ) {

		int prime = 31;
		int result = 1;
		result = ( prime * result ) + ( ( birthday == null )
										? 0
										: birthday.hashCode( ) );
		result = ( prime * result ) + ( ( email == null )
										? 0
										: email.hashCode( ) );
		result = ( prime * result ) + ( ( firstName == null )
										? 0
										: firstName.hashCode( ) );
		result = ( prime * result ) + ( gender
										? 1231
										: 1237 );
		result = ( prime * result ) + ( ( lastName == null )
										? 0
										: lastName.hashCode( ) );
		result = ( prime * result ) + ( ( photoUrl == null )
										? 0
										: photoUrl.toString( )
												  .hashCode( ) );
		result = ( prime * result ) + ( int ) ( trueNTHID ^ ( trueNTHID >>> 32 ) );
		result = ( prime * result ) + ( ( trueNTHUsername == null )
										? 0
										: trueNTHUsername.hashCode( ) );
		return result;
	}

	@Override
	public boolean equals( Object obj ) {

		if ( this == obj ) { return true; }
		if ( obj == null ) { return false; }
		if ( !( obj instanceof SSDemographicsImpl ) ) { return false; }
		SSDemographicsImpl other = ( SSDemographicsImpl ) obj;
		if ( birthday == null ) {
			if ( other.birthday != null ) { return false; }
		}
		else if ( !birthday.equals( other.birthday ) ) { return false; }
		if ( email == null ) {
			if ( other.email != null ) { return false; }
		}
		else if ( !email.equals( other.email ) ) { return false; }
		if ( firstName == null ) {
			if ( other.firstName != null ) { return false; }
		}
		else if ( !firstName.equals( other.firstName ) ) { return false; }
		if ( gender != other.gender ) { return false; }
		if ( lastName == null ) {
			if ( other.lastName != null ) { return false; }
		}
		else if ( !lastName.equals( other.lastName ) ) { return false; }
		if ( photoUrl == null ) {
			if ( other.photoUrl != null ) { return false; }
		}
		else if ( !photoUrl.equals( other.photoUrl ) ) { return false; }
		if ( trueNTHID != other.trueNTHID ) { return false; }
		if ( trueNTHUsername == null ) {
			return other.trueNTHUsername == null;
		}
		else { return trueNTHUsername.equals( other.trueNTHUsername ); }
	}

	@Override
	public Calendar getBirthday( ) {

		return birthday;
	}

	@Override
	public String getEmail( ) {

		return email;
	}

	@Override
	public String getFirstName( ) {

		return firstName;
	}

	/**
	 * Return the gender as a boolean value.
	 *
	 * @return <ul>
	 * <li>true, if its male;</li>
	 * <li>false, otherwise.</li>
	 * </ul>
	 */
	@Override
	public boolean getGender( ) {

		return gender;
	}

	@Override
	public String getLastName( ) {

		return lastName;
	}

	@Override
	public URL getPhotoUrl( ) {

		return photoUrl;
	}

	@Override
	public long getTrueNTHID( ) {

		return trueNTHID;
	}

	@Override
	public String getTrueNTHUsername( ) {

		return trueNTHUsername;
	}

	@Override
	public void setBirthday( Calendar birthday ) {

		this.birthday = birthday;
	}

	@Override
	public void setEmail( String email ) {

		this.email = email;
	}

	@Override
	public void setFirstName( String firstName ) {

		this.firstName = firstName;
	}

	/**
	 * Sets the gender attribute.
	 * <p>
	 * The gender is represented as a boolean flag: true for male, false for
	 * female.
	 * </p>
	 *
	 * @param isMale True for male, false for female.
	 */
	@Override
	public void setGender( boolean isMale ) {

		gender = isMale;
	}

	/**
	 * Sets the gender attribute.
	 * <p>
	 * The gender is represented as a boolean flag: true for male, false for
	 * female.
	 * </p>
	 *
	 * @param gender ("male"|"female").
	 */
	@Override
	public void setGender( String gender ) {

		String male = SSDemographicsProtocolProperties.GENDER_MALE.toString( );

		this.gender = male.equals( gender );
	}

	@Override
	public void setLastName( String lastName ) {

		this.lastName = lastName;
	}

	@Override
	public void setPhotoUrl( String url ) throws MalformedURLException {

		setPhotoUrl( ( url == null )
					 ? null
					 : new URL( url ) );
	}

	@Override
	public void setPhotoUrl( URL url ) {

		photoUrl = url;
	}

	@Override
	public void setTrueNTHID( String trueNTHID ) {

		this.trueNTHID = Long.parseLong( trueNTHID );
	}

	@Override
	public void setTrueNTHID( long trueNTHID ) {

		this.trueNTHID = trueNTHID;
	}

	@Override
	public void setTrueNTHUsername( String trueNTHUsername ) {

		this.trueNTHUsername = trueNTHUsername;
	}

}
