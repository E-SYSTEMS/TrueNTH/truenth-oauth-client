package edu.uw.cirg.truenth.oauth.client.internal.requests;

import edu.uw.cirg.truenth.oauth.client.TrueNTHOAuthClient;
import edu.uw.cirg.truenth.oauth.tokens.TrueNTHAccessToken;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import javax.json.JsonObject;
import java.io.IOException;

public final class TrueNTHJsonRequestUtil {

	public static JsonObject executeRequest( TrueNTHOAuthClient trueNTHOAuthClient, String address, TrueNTHAccessToken accessToken ) throws IOException {

		try (
				CloseableHttpClient httpClient = HttpClientBuilder.create( )
																  .build( )
		) {

			HttpGet httpGet = TrueNTHRequestFactory.getTrueNTHRequestGetJson( trueNTHOAuthClient, address, accessToken );

			return httpClient.execute( httpGet, new TrueNTHJsonResponseHandler( ) );
		}
	}

}
