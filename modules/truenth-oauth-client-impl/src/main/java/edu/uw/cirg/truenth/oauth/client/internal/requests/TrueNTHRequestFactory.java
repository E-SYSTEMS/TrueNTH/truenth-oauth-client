package edu.uw.cirg.truenth.oauth.client.internal.requests;

import edu.uw.cirg.truenth.oauth.client.TrueNTHOAuthClient;
import edu.uw.cirg.truenth.oauth.tokens.TrueNTHAccessToken;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.mime.MultipartEntityBuilder;

import static edu.uw.cirg.truenth.oauth.client.TrueNTHOAuthKeys.*;
import static org.apache.http.entity.ContentType.APPLICATION_JSON;
import static org.apache.http.entity.ContentType.MULTIPART_FORM_DATA;

final class TrueNTHRequestFactory {

	private static void addAcceptJsonHeader( HttpRequestBase request ) {

		request.addHeader( "accept", APPLICATION_JSON.getMimeType( ) );
	}

	private static void addFormData( MultipartEntityBuilder builder, String parameter, String value ) {

		builder.addTextBody( parameter, value, MULTIPART_FORM_DATA );
	}

	static HttpGet getTrueNTHRequestGet( TrueNTHOAuthClient trueNTHOAuthClient, String address, TrueNTHAccessToken accessToken ) {

		HttpGet httpGet = new HttpGet( address );

		trueNTHOAuthClient.signRequest( accessToken, httpGet );

		return httpGet;
	}

	static HttpGet getTrueNTHRequestGetJson( TrueNTHOAuthClient trueNTHOAuthClient, String address, TrueNTHAccessToken accessToken ) {

		HttpGet request = new HttpGet( address );

		addAcceptJsonHeader( request );

		trueNTHOAuthClient.signRequest( accessToken, request );

		return request;
	}

	static HttpPost getTrueNTHRequestPostJson( TrueNTHOAuthClient trueNTHOAuthClient, String address, TrueNTHAccessToken accessToken ) {

		HttpPost request = new HttpPost( address );

		addAcceptJsonHeader( request );

		trueNTHOAuthClient.signRequest( accessToken, request );

		return request;
	}

	static HttpPost getAccessTokenRequest( TrueNTHOAuthClient trueNTHOAuthClient, String code ) {

		String address = trueNTHOAuthClient.getAccessTokenEndpoint( );

		HttpPost request = new HttpPost( address );

		addAcceptJsonHeader( request );

		MultipartEntityBuilder builder = MultipartEntityBuilder.create( );

		addFormData( builder, OAUTH_CLIENT_ID, trueNTHOAuthClient.getApiKey( ) );
		addFormData( builder, OAUTH_CLIENT_SECRET, trueNTHOAuthClient.getApiSecret( ) );

		addFormData( builder, OAUTH_GRANT_TYPE, OAUTH_AUTHORIZATION_CODE );
		addFormData( builder, OAUTH_CODE, code );

		addFormData( builder, OAUTH_REDIRECT_URI, trueNTHOAuthClient.getCallback( ) );

		if ( trueNTHOAuthClient.hasScope( ) ) {
			addFormData( builder, OAUTH_SCOPE, trueNTHOAuthClient.getScope( ) );
		}

		request.setEntity( builder.build( ) );

		return request;
	}
}
