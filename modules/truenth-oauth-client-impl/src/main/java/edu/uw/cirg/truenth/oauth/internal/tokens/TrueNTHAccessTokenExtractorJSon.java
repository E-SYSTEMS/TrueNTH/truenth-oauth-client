package edu.uw.cirg.truenth.oauth.internal.tokens;

import edu.uw.cirg.truenth.oauth.client.TrueNTHOAuthKeys;
import edu.uw.cirg.truenth.oauth.tokens.TrueNTHAccessToken;
import edu.uw.cirg.truenth.oauth.tokens.TrueNTHAccessTokenExtractor;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonString;
import java.io.StringReader;
import java.time.Instant;

import static edu.uw.cirg.truenth.oauth.internal.tokens.Parameters.*;

public class TrueNTHAccessTokenExtractorJSon implements TrueNTHAccessTokenExtractor< JsonObject > {

	@Override
	public TrueNTHAccessToken extract( String data, Instant date ) {

		JsonObject json = Json.createReader( new StringReader( data ) )
							  .readObject( );
		return extract( json, date );
	}

	@Override
	public TrueNTHAccessToken extract( JsonObject data, Instant date ) {

		JsonString error = data.getJsonString( TrueNTHOAuthKeys.ERROR );
		if ( error != null ) { throw new IllegalArgumentException( "Error: " + error + "\n" + data ); }
		try {

			String accessToken = data.getString( ACCESS_TOKEN.toString( ) )
									 .trim( );

			int expiresIn = data.getJsonNumber( EXPIRES_IN.toString( ) )
								.intValue( );

			String refreshToken = data.getString( REFRESH_TOKEN.toString( ) )
									  .trim( );

			String scope = data.getString( SCOPE.toString( ) );

			return new TrueNTHAccessTokenImpl( accessToken, refreshToken, date, expiresIn, scope );
		}
		catch ( NullPointerException e ) {
			throw new IllegalArgumentException( "Error: data incomplete: " + data );
		}
	}

}
