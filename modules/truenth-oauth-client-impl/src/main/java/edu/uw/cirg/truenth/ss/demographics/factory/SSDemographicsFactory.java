package edu.uw.cirg.truenth.ss.demographics.factory;

import edu.uw.cirg.truenth.ss.demographics.SSDemographics;
import edu.uw.cirg.truenth.ss.internal.demographics.SSDemographicsImpl;

public final class SSDemographicsFactory {

	public static SSDemographics getSSDemographics( ) {

		return new SSDemographicsImpl( );
	}
}
