package edu.uw.cirg.truenth.oauth.client.builder.factory;

import edu.uw.cirg.truenth.oauth.client.builder.TrueNTHOAuthClientBuilder;
import edu.uw.cirg.truenth.oauth.client.internal.builder.TrueNTHOAuthClientBuilderImpl;

public final class TrueNTHOAuthClientBuilderFactory {

	public static TrueNTHOAuthClientBuilder getTrueNTHOAuthClientBuilder( ) {

		return new TrueNTHOAuthClientBuilderImpl( );
	}
}
