package edu.uw.cirg.truenth.oauth.client.internal;

import edu.uw.cirg.truenth.oauth.client.TrueNTHOAuthConfig;
import edu.uw.cirg.truenth.oauth.tokens.TrueNTHSignatureType;

public class TrueNTHOAuthConfigImpl implements TrueNTHOAuthConfig {

	private final String apiKey;
	private final String apiSecret;

	private final String callback;
	private final String scope;

	private final String rolesURL;
	private final TrueNTHSignatureType signatureType;

	private final String accessTokenEndpoint;
	private final String accessTokenStatusEndpoint;
	private final String baseAuthorizationURL;

	/**
	 * Shared Services base URL.
	 * <p>
	 * This URL points to Shared Services base URL, it should not be used for
	 * OAuth operation, but for fetching static resources, such as CSS. It is
	 * mainly used for templates and cases where the resource must come form the
	 * same SS instance.
	 * </p>
	 */
	private final String baseURL;

	/**
	 * Resource URL.
	 * <p>
	 * OAuth API base URL (API base).
	 * </p>
	 * <p>
	 * For instance: https://stg.us.truenth.org/api
	 * </p>
	 */
	private final String resourceURL;

	public TrueNTHOAuthConfigImpl( String apiKey, String apiSecret, String accessTokenEndpointURL, String accessTokenStatusEndpointURL, String baseAuthorizationURL, String baseURL, String resourceURL, String rolesURL, String callback, TrueNTHSignatureType signatureType, String scope ) {

		accessTokenEndpoint = accessTokenEndpointURL;
		accessTokenStatusEndpoint = accessTokenStatusEndpointURL;
		this.apiKey = apiKey;
		this.apiSecret = apiSecret;
		this.baseAuthorizationURL = baseAuthorizationURL;
		this.signatureType = signatureType;
		this.baseURL = baseURL;
		this.resourceURL = resourceURL;
		this.rolesURL = rolesURL;
		this.callback = callback;
		this.scope = scope;
	}

	@Override
	public boolean hasScope( ) {

		String scope = getScope( );
		return ( scope != null ) && !scope.isEmpty( );
	}

	@Override
	public String getAccessTokenEndpoint( ) {

		return accessTokenEndpoint;
	}

	@Override
	public String getAccessTokenStatusEndpoint( ) {

		return accessTokenStatusEndpoint;
	}

	@Override
	public String getApiKey( ) {

		return apiKey;
	}

	@Override
	public String getApiSecret( ) {

		return apiSecret;
	}

	@Override
	public String getBaseAuthorizationUrl( ) {

		return baseAuthorizationURL;
	}

	@Override
	public String getBaseUrl( ) {

		return baseURL;
	}

	@Override
	public String getCallback( ) {

		return callback;
	}

	@Override
	public String getResourceUrl( ) {

		return resourceURL;
	}

	@Override
	public String getRolesUrl( ) {

		return rolesURL;
	}

	@Override
	public String getScope( ) {

		return scope;
	}

	@Override
	public TrueNTHSignatureType getSignatureType( ) {

		return signatureType;
	}
}
