package edu.uw.cirg.truenth.oauth.internal.signedrequest;

import edu.uw.cirg.truenth.oauth.signedrequest.TrueNTHSignedRequest;
import edu.uw.cirg.truenth.oauth.tokens.TrueNTHRefreshToken;
import edu.uw.cirg.truenth.oauth.tokens.factory.TrueNTHAccessTokenFactory;
import org.apache.commons.codec.binary.Base64;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.json.Json;
import javax.json.JsonObject;
import java.io.StringReader;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import static java.nio.charset.StandardCharsets.UTF_8;

public class TrueNTHSignedRequestImpl implements TrueNTHSignedRequest {

	private final JsonObject data;
	private final String signature;
	private final static String HMAC_SHA256_LOCAL = "HmacSHA256";
	private final static String HMAC_SHA256_SS = "HMAC-SHA256";
	private final static Base64 base64 = new Base64( false );

	public TrueNTHSignedRequestImpl( String signedRequest, String validationKey ) throws InvalidKeyException, NoSuchAlgorithmException {

		if ( signedRequest == null ) { throw new NullPointerException( "signed_request cannot be null" ); }

		String[] signedRequestParts = signedRequest.split( "\\.", 2 );

		signature = new String( base64.decode( signedRequestParts[ 0 ].getBytes( UTF_8 ) ), UTF_8 );

		String rawData = signedRequestParts[ 1 ];

		String dataString = new String( base64.decode( rawData.getBytes( UTF_8 ) ), UTF_8 );

		data = Json.createReader( new StringReader( dataString ) )
				   .readObject( );

		validate( rawData, validationKey );
	}

	private String hmacSHA256( String data, String key ) throws NoSuchAlgorithmException, InvalidKeyException {

		SecretKeySpec secretKey = new SecretKeySpec( key.getBytes( UTF_8 ), HMAC_SHA256_LOCAL );
		Mac mac = Mac.getInstance( HMAC_SHA256_LOCAL );
		mac.init( secretKey );
		byte[] hmacData = mac.doFinal( data.getBytes( UTF_8 ) );
		return new String( hmacData, UTF_8 );
	}

	private void validate( String rawData, String validationKey ) throws NoSuchAlgorithmException, InvalidKeyException {

		if ( !data.getString( ALGORITHM )
				  .equals( HMAC_SHA256_SS ) ) { throw new NoSuchAlgorithmException( "Unknown signature algorithm." ); }

		if ( !hmacSHA256( rawData, validationKey ).equals( signature ) ) { throw new IllegalStateException( "Signature is not correct." ); }
	}

	@Override
	public JsonObject getData( ) {

		return data;
	}

	@Override
	public String getSignature( ) {

		return signature;
	}

	@Override
	public String getUserId( ) {

		return getData( ).getString( USER_ID );
	}

	@Override
	public String getAlgorithm( ) {

		return getData( ).getString( ALGORITHM );
	}

	@Override
	public String getIssuedAt( ) {

		return getData( ).getString( ISSUED_AT );
	}

	@Override
	public String getEvent( ) {

		return getData( ).getString( EVENT );
	}

	@Override
	public TrueNTHRefreshToken getRefreshToken( ) {

		return TrueNTHAccessTokenFactory.getTrueNTHRefreshToken( getData( ).getString( REFRESH_TOKEN ) );
	}

}
