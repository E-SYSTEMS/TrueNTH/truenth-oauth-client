package edu.uw.cirg.truenth.ss.internal.demographics;

public enum SSDemographicsProtocolProperties {

	BIRTH_DATE( "birthDate" ), COMMUNICATION( "communication" ), GENDER( "gender" ), GENDER_FEMALE( "female" ), GENDER_MALE( "male" ), IDENTIFIER( "identifier" ), IDENTIFIER_SYSTEM( "system" ), IDENTIFIER_TRUENTH_SYSTEM( "http://us.truenth.org/identity-codes/TrueNTH-identity" ), IDENTIFIER_TRUENTH_SYSTEM_USERNAME( "http://us.truenth.org/identity-codes/TrueNTH-username" ), IDENTIFIER_VALUE( "value" ), NAME( "name" ), NAME_FAMILY( "family" ), NAME_GIVEN( "given" ), PHOTO( "photo" ), PHOTO_URL( "url" ), TELECOM( "telecom" ), TELECOM_SYSTEM( "system" ), TELECOM_SYSTEM_EMAIL( "email" ), TELECOM_SYSTEM_VALUE( "value" );

	private final String propertyName;

	SSDemographicsProtocolProperties( String propName ) {

		propertyName = propName;
	}

	@Override
	public String toString( ) {

		return propertyName;
	}

}
