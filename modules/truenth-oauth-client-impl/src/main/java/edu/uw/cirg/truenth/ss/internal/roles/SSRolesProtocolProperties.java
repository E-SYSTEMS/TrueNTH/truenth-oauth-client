package edu.uw.cirg.truenth.ss.internal.roles;

public enum SSRolesProtocolProperties {

	DESCRIPTION( "description" ), NAME( "name" ), ROOT( "roles" );

	private final String propertyName;

	SSRolesProtocolProperties( String propName ) {

		propertyName = propName;
	}

	@Override
	public String toString( ) {

		return propertyName;
	}
}
