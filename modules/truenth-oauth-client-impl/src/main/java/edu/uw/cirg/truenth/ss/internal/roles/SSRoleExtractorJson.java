package edu.uw.cirg.truenth.ss.internal.roles;

import edu.uw.cirg.truenth.ss.roles.SSRole;
import edu.uw.cirg.truenth.ss.roles.SSRoleExtractor;

import javax.json.JsonArray;
import javax.json.JsonObject;
import java.util.ArrayList;
import java.util.List;

public class SSRoleExtractorJson implements SSRoleExtractor< JsonObject > {

	@Override
	public String extractDescription( JsonObject data ) {

		return data.getString( SSRolesProtocolProperties.DESCRIPTION.toString( ) );
	}

	@Override
	public String extractName( JsonObject data ) {

		return data.getString( SSRolesProtocolProperties.NAME.toString( ) );
	}

	@Override
	public SSRole extractRole( JsonObject data ) {

		SSRole role = new SSRoleImpl( );
		role.setName( extractName( data ) );
		role.setDescription( extractDescription( data ) );
		return role;
	}

	@Override
	public List< SSRole > extractRoles( JsonObject data ) {

		JsonArray rolesData = data.getJsonArray( SSRolesProtocolProperties.ROOT.toString( ) );
		if ( rolesData == null ) { return null; }

		ArrayList< SSRole > roleList = new ArrayList<>( rolesData.size( ) );

		SSRole role;

		for ( JsonObject roleData : rolesData.getValuesAs( JsonObject.class ) ) {
			role = extractRole( roleData );
			roleList.add( role );
		}

		return roleList;
	}
}
