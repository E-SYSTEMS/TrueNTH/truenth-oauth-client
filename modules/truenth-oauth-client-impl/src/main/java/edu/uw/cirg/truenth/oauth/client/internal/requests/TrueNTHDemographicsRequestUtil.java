package edu.uw.cirg.truenth.oauth.client.internal.requests;

import edu.uw.cirg.truenth.oauth.client.TrueNTHOAuthClient;
import edu.uw.cirg.truenth.oauth.tokens.TrueNTHAccessToken;
import edu.uw.cirg.truenth.ss.demographics.SSDemographics;
import edu.uw.cirg.truenth.ss.demographics.SSDemographicsExtractor;
import edu.uw.cirg.truenth.ss.internal.demographics.SSDemographicsExtractorJson;

import javax.json.JsonObject;
import java.io.IOException;

public final class TrueNTHDemographicsRequestUtil {

	private static final SSDemographicsExtractor< JsonObject > demographicsExtractor = new SSDemographicsExtractorJson( );

	public static SSDemographics executeRequest( TrueNTHOAuthClient trueNTHOAuthClient, TrueNTHAccessToken accessToken ) throws IOException {

		String address = trueNTHOAuthClient.getResourceDemographicsUrl( );

		JsonObject data = TrueNTHJsonRequestUtil.executeRequest( trueNTHOAuthClient, address, accessToken );

		return demographicsExtractor.extractDemographics( data );

	}

}
