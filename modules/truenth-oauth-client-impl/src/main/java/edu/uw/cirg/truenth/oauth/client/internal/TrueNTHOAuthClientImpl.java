package edu.uw.cirg.truenth.oauth.client.internal;

import edu.uw.cirg.truenth.oauth.client.TrueNTHOAuthClient;
import edu.uw.cirg.truenth.oauth.client.TrueNTHOAuthConfig;
import edu.uw.cirg.truenth.oauth.client.builder.TrueNTHOAuthProvider;
import edu.uw.cirg.truenth.oauth.client.internal.requests.*;
import edu.uw.cirg.truenth.oauth.tokens.TrueNTHAccessToken;
import edu.uw.cirg.truenth.ss.demographics.SSDemographics;
import edu.uw.cirg.truenth.ss.roles.SSRole;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpRequestBase;

import javax.json.JsonObject;
import java.io.IOException;
import java.util.List;

import static edu.uw.cirg.truenth.oauth.client.TrueNTHOAuthKeys.SS_PATH_DEMOGRAPHICS;
import static edu.uw.cirg.truenth.oauth.client.TrueNTHUrlPlaceHolders.USER_ID;

public class TrueNTHOAuthClientImpl implements TrueNTHOAuthClient {

	private final TrueNTHOAuthProvider provider;
	private final TrueNTHOAuthConfig config;
	private static final String VERSION = "2.0";

	public TrueNTHOAuthClientImpl( TrueNTHOAuthProvider trueNTHOAuthProvider, TrueNTHOAuthConfig config ) {

		provider = trueNTHOAuthProvider;
		this.config = config;
	}

	@Override
	public TrueNTHAccessToken getAccessToken( String code ) throws IOException {

		return TrueNTHAccessTokenRequestUtil.executeRequest( this, code );
	}

	@Override
	public String getAccessTokenEndpoint( ) {

		return provider.getAccessTokenEndpoint( config );
	}

	@Override
	public TrueNTHAccessToken getAccessTokenStatus( TrueNTHAccessToken accessToken ) throws IOException {

		return TrueNTHAccessTokenStatusRequestUtil.executeRequest( this, accessToken );

	}

	@Override
	public String getAuthorizationUrl( List< NameValuePair > callbackParameters, List< NameValuePair > parameters ) {

		return provider.getAuthorizationUrl( config, callbackParameters, parameters );
	}

	@Override
	public String getAuthorizationUrl( List< NameValuePair > callbackParameters ) {

		return getAuthorizationUrl( callbackParameters, null );
	}

	@Override
	public String getResource( String path, TrueNTHAccessToken accessToken ) throws IOException {

		String url = getResourceUrl( path );

		return TrueNTHStringRequestUtil.executeRequest( this, url, accessToken );

	}

	@Override
	public SSDemographics getResourceDemographics( TrueNTHAccessToken accessToken ) throws IOException {

		return TrueNTHDemographicsRequestUtil.executeRequest( this, accessToken );
	}

	@Override
	public JsonObject getResourceJson( String path, TrueNTHAccessToken accessToken ) throws IOException {

		String url = getResourceUrl( path );

		return TrueNTHJsonRequestUtil.executeRequest( this, url, accessToken );

	}

	@Override
	public String getResourceDemographicsUrl( ) {

		return getResourceUrl( SS_PATH_DEMOGRAPHICS );
	}

	@Override
	public String getResourceUrl( String path ) {

		return provider.getResourceUrl( config, SS_PATH_DEMOGRAPHICS );
	}

	@Override
	public String getRolesUrl( long userId ) {

		return provider.getRolesUrl( config, userId )
					   .replaceFirst( USER_ID, String.valueOf( userId ) );
	}

	@Override
	public List< SSRole > getTrueNTHRoles( long trueNTHUserId, TrueNTHAccessToken accessToken ) throws IOException {

		return TrueNTHRolesRequestUtil.executeRequest( this, trueNTHUserId, accessToken );

	}

	@Override
	public boolean hasScope( ) {

		return config.hasScope( );
	}

	@Override
	public boolean isAccessTokenActive( TrueNTHAccessToken accessToken ) throws IOException {

		if ( accessToken.isExpired( ) ) { return false; }

		TrueNTHAccessToken token = TrueNTHAccessTokenStatusRequestUtil.executeRequest( this, accessToken );

		return token != null && token.getExpiresIn( ) > 0;

	}

	@Override
	public void signRequest( TrueNTHAccessToken accessToken, HttpRequestBase request ) {

		provider.signRequest( config, accessToken, request );
	}

	@Override
	public String getAccessTokenStatusEndpoint( ) {

		return provider.getAccessTokenStatusEndpoint( config );
	}

	@Override
	public String getApiKey( ) {

		return config.getApiKey( );
	}

	@Override
	public String getApiSecret( ) {

		return config.getApiSecret( );
	}

	@Override
	public String getAuthorizationUrl( ) {

		return provider.getAuthorizationUrl( config );
	}

	@Override
	public String getCallback( ) {

		return provider.getCallback( config );
	}

	@Override
	public String getScope( ) {

		return config.getScope( );
	}

	@Override
	public TrueNTHOAuthConfig getTrueNTHOAuthConfig( ) {

		return config;
	}

	@Override
	public String getVersion( ) {

		return VERSION;
	}

}
