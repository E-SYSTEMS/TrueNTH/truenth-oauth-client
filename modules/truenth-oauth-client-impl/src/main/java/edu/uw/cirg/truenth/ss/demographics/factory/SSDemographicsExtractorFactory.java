package edu.uw.cirg.truenth.ss.demographics.factory;

import edu.uw.cirg.truenth.ss.demographics.SSDemographicsExtractor;
import edu.uw.cirg.truenth.ss.internal.demographics.SSDemographicsExtractorJson;

import javax.json.JsonObject;

public final class SSDemographicsExtractorFactory {

	public static SSDemographicsExtractor< JsonObject > getSSDemographicsExtractorJson( ) {

		return new SSDemographicsExtractorJson( );
	}
}
