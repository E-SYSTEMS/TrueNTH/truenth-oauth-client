package edu.uw.cirg.truenth.oauth.internal.tokens;

import static edu.uw.cirg.truenth.oauth.client.TrueNTHOAuthKeys.*;

enum Parameters {
	ACCESS_TOKEN( OAUTH_ACCESS_TOKEN ), EXPIRES_IN( OAUTH_EXPIRES_IN ), REFRESH_TOKEN( OAUTH_REFRESH_TOKEN ), SCOPE( OAUTH_SCOPE ), SCOPES( OAUTH_SCOPES ), TOKEN_TYPE( OAUTH_TOKEN_TYPE );

	private final String parameter;

	Parameters( String parameter ) {

		this.parameter = parameter;
	}

	@Override
	public String toString( ) {

		return parameter;
	}
}
