package edu.uw.cirg.truenth.oauth.client.internal.builder;

import edu.uw.cirg.truenth.oauth.client.TrueNTHOAuthClient;
import edu.uw.cirg.truenth.oauth.client.builder.TrueNTHOAuthClientBuilder;
import edu.uw.cirg.truenth.oauth.client.builder.TrueNTHOAuthProvider;
import edu.uw.cirg.truenth.oauth.client.internal.TrueNTHOAuthConfigImpl;
import edu.uw.cirg.truenth.oauth.tokens.TrueNTHSignatureType;

import static edu.uw.cirg.truenth.oauth.tokens.TrueNTHSignatureType.BEARER_AUTHORIZATION_REQUEST_HEADER_FIELD;

public class TrueNTHOAuthClientBuilderImpl implements TrueNTHOAuthClientBuilder {

	private String accessTokenEndpointURL;
	private String accessTokenStatusEndpointURL;
	private String apiKey;
	private String apiSecret;
	private String baseAuthorizationURL;
	private String baseURL;
	private String callbackURL;
	private String resourceURL;
	private String rolesURL;
	private String scope;
	private TrueNTHSignatureType signatureType;
	private static final TrueNTHOAuthProvider PROVIDER = new TrueNTHOAuthProviderImpl( );

	public TrueNTHOAuthClientBuilderImpl( ) {

		scope = "email";
		signatureType = BEARER_AUTHORIZATION_REQUEST_HEADER_FIELD;
	}

	@Override
	public TrueNTHOAuthClientBuilder accessTokenEndpointURL( String accessTokenEndpointURL ) {

		this.accessTokenEndpointURL = accessTokenEndpointURL;
		return this;
	}

	@Override
	public TrueNTHOAuthClientBuilder accessTokenStatusEndpointURL( String accessTokenStatusEndpointURL ) {

		this.accessTokenStatusEndpointURL = accessTokenStatusEndpointURL;
		return this;
	}

	@Override
	public TrueNTHOAuthClientBuilder apiKey( String apiKey ) {

		this.apiKey = apiKey;
		return this;
	}

	@Override
	public TrueNTHOAuthClientBuilder apiSecret( String apiSecret ) {

		this.apiSecret = apiSecret;
		return this;
	}

	@Override
	public TrueNTHOAuthClientBuilder baseAuthorizationURL( String baseAuthorizationURL ) {

		this.baseAuthorizationURL = baseAuthorizationURL;
		return this;
	}

	@Override
	public TrueNTHOAuthClientBuilder baseURL( String baseURL ) {

		this.baseURL = baseURL;
		return this;
	}

	@Override
	public TrueNTHOAuthClient build( ) {

		return PROVIDER.createService( new TrueNTHOAuthConfigImpl( getApiKey( ), getApiSecret( ), getAccessTokenEndpointURL( ), getAccessTokenStatusEndpointURL( ), getBaseAuthorizationURL( ), getBaseURL( ), getResourceURL( ), getRolesURL( ), getCallbackURL( ), getSignatureType( ), getScope( ) ) );
	}

	@Override
	public TrueNTHOAuthClientBuilder callbackURL( String callback ) {

		callbackURL = callback;
		return this;
	}

	@Override
	public TrueNTHOAuthClientBuilder resourceURL( String resourceURL ) {

		this.resourceURL = resourceURL;
		return this;
	}

	@Override
	public TrueNTHOAuthClientBuilder rolesURL( String rolesURL ) {

		this.rolesURL = rolesURL;
		return this;
	}

	@Override
	public TrueNTHOAuthClientBuilder scope( String scope ) {

		this.scope = scope;
		return this;
	}

	@Override
	public String getAccessTokenEndpointURL( ) {

		return accessTokenEndpointURL;
	}

	@Override
	public String getAccessTokenStatusEndpointURL( ) {

		return accessTokenStatusEndpointURL;
	}

	@Override
	public String getApiKey( ) {

		return apiKey;
	}

	@Override
	public String getApiSecret( ) {

		return apiSecret;
	}

	@Override
	public String getBaseAuthorizationURL( ) {

		return baseAuthorizationURL;
	}

	/**
	 * Returns the Shared Services' base URL.
	 * <p>
	 * This URL points to Shared Services' base URL, it should not be used for
	 * OAuth operations, but for fetching static resources, such as CSS. It is
	 * mainly used for templates.
	 * </p>
	 * <p>
	 * Example: https://stg.us.truenth.org (staging server).
	 * </p>
	 *
	 * @return The SS' base URL.
	 */
	@Override
	public String getBaseURL( ) {

		return baseURL;
	}

	@Override
	public String getCallbackURL( ) {

		return callbackURL;
	}

	@Override
	public String getResourceURL( ) {

		return resourceURL;
	}

	@Override
	public String getRolesURL( ) {

		return rolesURL;
	}

	@Override
	public String getScope( ) {

		return scope;
	}

	@Override
	public TrueNTHSignatureType getSignatureType( ) {

		return signatureType;
	}

	public TrueNTHOAuthClientBuilder signatureType( TrueNTHSignatureType type ) {

		signatureType = type;
		return this;
	}

}
