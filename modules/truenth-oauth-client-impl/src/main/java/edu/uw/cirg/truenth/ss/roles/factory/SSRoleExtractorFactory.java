package edu.uw.cirg.truenth.ss.roles.factory;

import edu.uw.cirg.truenth.ss.internal.roles.SSRoleExtractorJson;
import edu.uw.cirg.truenth.ss.roles.SSRoleExtractor;

import javax.json.JsonObject;

public final class SSRoleExtractorFactory {

	public static SSRoleExtractor< JsonObject > getSSRoleExtractorJson( ) {

		return new SSRoleExtractorJson( );
	}
}
