package edu.uw.cirg.truenth.ss.internal.demographics;

import edu.uw.cirg.truenth.ss.demographics.SSDemographics;
import edu.uw.cirg.truenth.ss.demographics.SSDemographicsExtractor;

import javax.json.*;
import javax.json.JsonValue.ValueType;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class SSDemographicsExtractorJson implements SSDemographicsExtractor< JsonObject > {

	@Override
	public Calendar extractBirthday( JsonObject data ) {

		Calendar birthday = Calendar.getInstance( );

		if ( data == null ) { return birthday; }

		JsonString date = data.getJsonString( SSDemographicsProtocolProperties.BIRTH_DATE.toString( ) );

		if ( date == null ) { return birthday; }

		try {
			SimpleDateFormat birthdayFormat = new SimpleDateFormat( "yyyy-MM-dd" );
			birthday.setTime( birthdayFormat.parse( date.getString( ) ) );
			return birthday;
		}
		catch ( ParseException e ) {
			return birthday;
		}
	}

	@Override
	public SSDemographics extractDemographics( JsonObject data ) {

		SSDemographics demographics = new SSDemographicsImpl( );
		demographics.setBirthday( extractBirthday( data ) );
		demographics.setEmail( extractEmail( data ) );
		demographics.setFirstName( extractFirstName( data ) );
		demographics.setLastName( extractLastName( data ) );
		demographics.setGender( extractGender( data ) );
		demographics.setPhotoUrl( extractPhotoUrl( data ) );
		demographics.setTrueNTHUsername( extractTrueNTHUsername( data ) );
		demographics.setTrueNTHID( extractTrueNTHId( data ) );
		return demographics;
	}

	@Override
	public String extractEmail( JsonObject data ) {

		if ( data == null ) { return null; }

		JsonArray telecoms = data.getJsonArray( SSDemographicsProtocolProperties.TELECOM.toString( ) );

		if ( telecoms == null ) { return null; }

		for ( int index = 0; index < telecoms.size( ); index++ ) {
			JsonObject telcom = telecoms.getJsonObject( index );
			String system = telcom.getString( SSDemographicsProtocolProperties.TELECOM_SYSTEM.toString( ) );

			if ( SSDemographicsProtocolProperties.TELECOM_SYSTEM_EMAIL.toString( )
																	  .equals( system ) ) { return telcom.getString( SSDemographicsProtocolProperties.TELECOM_SYSTEM_VALUE.toString( ) ); }
		}

		return null;
	}

	@Override
	public String extractFirstName( JsonObject data ) {

		if ( data == null ) { return null; }

		JsonObject name = data.getJsonObject( SSDemographicsProtocolProperties.NAME.toString( ) );
		if ( name == null ) { return null; }

		JsonString firstName = name.getJsonString( SSDemographicsProtocolProperties.NAME_GIVEN.toString( ) );
		return ( firstName != null )
			   ? firstName.getString( )
			   : null;
	}

	@Override
	public String extractGender( JsonObject data ) {

		if ( data == null ) { return null; }

		JsonString rawGender = data.getJsonString( SSDemographicsProtocolProperties.GENDER.toString( ) );

		if ( rawGender == null ) { return null; }

		String gender = rawGender.getString( );

		if ( SSDemographicsProtocolProperties.GENDER_MALE.toString( )
														 .equals( gender ) || SSDemographicsProtocolProperties.GENDER_FEMALE.toString( )
																															.equals( gender ) ) { return gender; }

		return null;
	}

	@Override
	public String extractLastName( JsonObject data ) {

		if ( data == null ) { return null; }

		JsonObject name = data.getJsonObject( SSDemographicsProtocolProperties.NAME.toString( ) );

		if ( name == null ) { return null; }

		JsonString lastName = name.getJsonString( SSDemographicsProtocolProperties.NAME_FAMILY.toString( ) );
		return ( lastName != null )
			   ? lastName.getString( )
			   : null;
	}

	@Override
	public URL extractPhotoUrl( JsonObject data ) {

		if ( data == null ) { return null; }

		JsonArray photo = data.getJsonArray( SSDemographicsProtocolProperties.PHOTO.toString( ) );

		if ( ( photo == null ) || photo.isEmpty( ) ) { return null; }

		try {
			return new URL( photo.getJsonObject( 0 )
								 .getString( SSDemographicsProtocolProperties.PHOTO_URL.toString( ) ) );
		}
		catch ( Exception ex ) {
			return null;
		}

	}

	/**
	 * Extracts: TrueNTH ID.
	 * <p>
	 * Input format:
	 * </p>
	 * <p>
	 * <pre>
	 * IDENTIFIER: [
	 *     {
	 * 		IDENTIFIER_SYSTEM: IDENTIFIER_TRUENTH_SYSTEM,
	 * 		<b>IDENTIFIER_VALUE: "9999"</b>
	 *     },
	 *     {
	 * 		IDENTIFIER_SYSTEM: IDENTIFIER_TRUENTH_SYSTEM_USERNAME,
	 * 		IDENTIFIER_VALUE: "username"
	 *     }
	 * ]
	 * </pre>
	 * <p>
	 * Input Example:
	 * <p>
	 * <pre>
	 * "identifier": [
	 *       {
	 *           "system": "http://us.truenth.org/identity-codes/TrueNTH-identity",
	 *           <b>"value": "10015"</b>
	 *       },
	 *       {
	 *           "system": "http://us.truenth.org/identity-codes/TrueNTH-username",
	 *           "value": "Bob Truenth"
	 *       }
	 *  ]
	 * </pre>
	 *
	 * @param data Data origin.
	 * @return <ul>
	 * <li>TrueNTH ID, if it can be extracted;</li>
	 * <li>null, otherwise.</li>
	 * </ul>
	 */
	@Override
	public String extractTrueNTHId( JsonObject data ) {

		if ( data == null ) { return null; }

		JsonArray idArray = data.getJsonArray( SSDemographicsProtocolProperties.IDENTIFIER.toString( ) );

		if ( idArray == null ) { return null; }

		for ( int index = 0; index < idArray.size( ); index++ ) {

			JsonObject id = idArray.getJsonObject( index );

			String idType = id.getString( SSDemographicsProtocolProperties.IDENTIFIER_SYSTEM.toString( ) );

			if ( SSDemographicsProtocolProperties.IDENTIFIER_TRUENTH_SYSTEM.toString( )
																		   .equals( idType ) ) {
				JsonValue truenthId = id.get( SSDemographicsProtocolProperties.IDENTIFIER_VALUE.toString( ) );
				if ( truenthId.getValueType( )
							  .equals( ValueType.STRING ) ) { return ( ( JsonString ) truenthId ).getString( ); }
				else if ( truenthId.getValueType( )
								   .equals( ValueType.NUMBER ) ) { return Long.toString( ( ( JsonNumber ) truenthId ).longValueExact( ) ); }

			}
		}

		return null;
	}

	/**
	 * Extracts: TrueNTH Username.
	 * <p>
	 * Input format:
	 * </p>
	 * <p>
	 * <pre>
	 * IDENTIFIER: [
	 *     {
	 * 		IDENTIFIER_SYSTEM: IDENTIFIER_TRUENTH_SYSTEM,
	 * 		<b>IDENTIFIER_VALUE: 9999</b>
	 *     },
	 *     {
	 * 		IDENTIFIER_SYSTEM: IDENTIFIER_TRUENTH_SYSTEM_USERNAME,
	 * 		IDENTIFIER_VALUE: "username"
	 *     }
	 * ]
	 * </pre>
	 * <p>
	 * Input Example:
	 * <p>
	 * <pre>
	 * "identifier": [
	 *       {
	 *           "system": "http://us.truenth.org/identity-codes/TrueNTH-identity",
	 *           <b>"value": 10015</b>
	 *       },
	 *       {
	 *           "system": "http://us.truenth.org/identity-codes/TrueNTH-username",
	 *           "value": "Bob Truenth"
	 *       }
	 *  ]
	 * </pre>
	 *
	 * @param data Data origin.
	 * @return <ul>
	 * <li>TrueNTH username, if it can be extracted;</li>
	 * <li>null, otherwise.</li>
	 * </ul>
	 */
	@Override
	public String extractTrueNTHUsername( JsonObject data ) {

		if ( data == null ) { return null; }

		JsonArray idArray = data.getJsonArray( SSDemographicsProtocolProperties.IDENTIFIER.toString( ) );

		if ( idArray == null ) { return null; }

		for ( int index = 0; index < idArray.size( ); index++ ) {

			JsonObject id = idArray.getJsonObject( index );

			String idType = id.getString( SSDemographicsProtocolProperties.IDENTIFIER_SYSTEM.toString( ) );

			if ( SSDemographicsProtocolProperties.IDENTIFIER_TRUENTH_SYSTEM_USERNAME.toString( )
																					.equals( idType ) ) { return id.getString( SSDemographicsProtocolProperties.IDENTIFIER_VALUE.toString( ) ); }
		}

		return null;
	}
}
