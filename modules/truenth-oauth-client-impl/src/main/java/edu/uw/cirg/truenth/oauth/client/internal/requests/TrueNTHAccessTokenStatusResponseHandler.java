package edu.uw.cirg.truenth.oauth.client.internal.requests;

import edu.uw.cirg.truenth.oauth.internal.tokens.TrueNTHAccessTokenExtractorJSon;
import edu.uw.cirg.truenth.oauth.tokens.TrueNTHAccessToken;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.ResponseHandler;

import javax.json.Json;
import javax.json.JsonObject;
import java.io.IOException;
import java.time.Instant;

import static edu.uw.cirg.truenth.oauth.tokens.factory.TrueNTHAccessTokenFactory.getExpired;
import static java.time.format.DateTimeFormatter.RFC_1123_DATE_TIME;

class TrueNTHAccessTokenStatusResponseHandler implements ResponseHandler< TrueNTHAccessToken > {

	private final TrueNTHAccessToken accessToken;
	private static final TrueNTHAccessTokenExtractorJSon tokenExtractor = new TrueNTHAccessTokenExtractorJSon( );

	public TrueNTHAccessTokenStatusResponseHandler( TrueNTHAccessToken accessToken ) {

		this.accessToken = accessToken;
	}

	@Override
	public TrueNTHAccessToken handleResponse( HttpResponse response ) throws IOException {

		int status = response.getStatusLine( )
							 .getStatusCode( );

		HttpEntity entity = response.getEntity( );

		if ( ( status >= 200 ) && ( status < 300 ) ) {

			Instant date = RFC_1123_DATE_TIME.parse( response.getFirstHeader( "Date" )
															 .getValue( ), Instant::from );

			JsonObject tokenData = Json.createReader( entity.getContent( ) )
									   .readObject( );

			return tokenExtractor.extract( tokenData, date );

		}
		else if ( status == 401 ) {
			return getExpired( accessToken );
		}
		else {
			throw new HttpResponseException( status, "Unexpected response status: " + response.getStatusLine( ) );
		}

	}

}
