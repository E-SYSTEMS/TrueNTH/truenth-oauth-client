package edu.uw.cirg.truenth.ss.internal.roles;

import edu.uw.cirg.truenth.ss.roles.SSRole;

public class SSRoleImpl implements SSRole {

	private String description;
	private String name;
	private static final long serialVersionUID = 1L;

	public SSRoleImpl( ) {

	}

	public SSRoleImpl( String ssName, String ssDescription ) {

		name = ssName;
		description = ssDescription;
	}

	@Override
	public String getDescription( ) {

		return description;
	}

	@Override
	public String getName( ) {

		return name;
	}

	@Override
	public void setDescription( String ssDescription ) {

		description = ssDescription;
	}

	@Override
	public void setName( String ssName ) {

		name = ssName;
	}

}
