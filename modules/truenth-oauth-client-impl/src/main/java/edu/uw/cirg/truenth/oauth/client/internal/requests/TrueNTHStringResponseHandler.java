package edu.uw.cirg.truenth.oauth.client.internal.requests;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

class TrueNTHStringResponseHandler implements ResponseHandler< String > {

	@Override
	public String handleResponse( HttpResponse response ) throws IOException {

		int status = response.getStatusLine( )
							 .getStatusCode( );

		if ( ( status >= 200 ) && ( status < 300 ) ) {

			HttpEntity entity = response.getEntity( );

			if ( entity == null ) {
				throw new HttpResponseException( status, "Empty response entity" );
			}

			return EntityUtils.toString( entity );

		}

		else {
			throw new HttpResponseException( status, "Unexpected response status: " + response.getStatusLine( ) );
		}

	}

}
