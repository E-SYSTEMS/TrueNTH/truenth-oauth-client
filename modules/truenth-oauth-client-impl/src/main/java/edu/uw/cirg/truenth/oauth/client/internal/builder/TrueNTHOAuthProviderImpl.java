package edu.uw.cirg.truenth.oauth.client.internal.builder;

import edu.uw.cirg.truenth.oauth.client.TrueNTHOAuthClient;
import edu.uw.cirg.truenth.oauth.client.TrueNTHOAuthConfig;
import edu.uw.cirg.truenth.oauth.client.builder.TrueNTHOAuthProvider;
import edu.uw.cirg.truenth.oauth.client.internal.TrueNTHOAuthClientImpl;
import edu.uw.cirg.truenth.oauth.tokens.TrueNTHAccessToken;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URIBuilder;

import java.net.URISyntaxException;
import java.util.List;

import static edu.uw.cirg.truenth.oauth.client.TrueNTHOAuthKeys.*;
import static edu.uw.cirg.truenth.oauth.client.TrueNTHUrlPlaceHolders.USER_ID;
import static edu.uw.cirg.truenth.oauth.tokens.TrueNTHTokenType.BEARER;

class TrueNTHOAuthProviderImpl implements TrueNTHOAuthProvider {

	@Override
	public TrueNTHOAuthClient createService( TrueNTHOAuthConfig config ) {

		return new TrueNTHOAuthClientImpl( this, config );
	}

	@Override
	public String getAccessTokenEndpoint( TrueNTHOAuthConfig config ) {

		return config.getAccessTokenEndpoint( );
	}

	@Override
	public String getAccessTokenStatusEndpoint( TrueNTHOAuthConfig config ) {

		return config.getAccessTokenStatusEndpoint( );
	}

	@Override
	public String getAuthorizationUrl( TrueNTHOAuthConfig config ) {

		return getAuthorizationUrl( config, null, null );

	}

	@Override
	public String getAuthorizationUrl( TrueNTHOAuthConfig config, List< NameValuePair > callbackParameters, List< NameValuePair > parameters ) {

		try {
			URIBuilder callbackUrlBuilder = new URIBuilder( config.getCallback( ) );
			URIBuilder authorizationUrlBuilder = new URIBuilder( config.getBaseAuthorizationUrl( ) );

			if ( callbackParameters != null ) {
				callbackUrlBuilder.addParameters( callbackParameters );
			}

			authorizationUrlBuilder.addParameter( OAUTH_RESPONSE_TYPE, OAUTH_RESPONSE_TYPE_CODE );
			authorizationUrlBuilder.addParameter( OAUTH_CLIENT_ID, config.getApiKey( ) );

			if ( config.hasScope( ) ) {
				authorizationUrlBuilder.addParameter( OAUTH_SCOPE, config.getScope( ) );
			}

			if ( parameters != null ) { authorizationUrlBuilder.addParameters( parameters ); }

			authorizationUrlBuilder.addParameter( OAUTH_REDIRECT_URI, callbackUrlBuilder.build( )
																						.toString( ) );

			return authorizationUrlBuilder.build( )
										  .toString( );
		}
		catch ( URISyntaxException e ) {
			throw new RuntimeException( e );
		}
	}

	@Override
	public String getCallback( TrueNTHOAuthConfig config ) {

		return config.getCallback( );
	}

	@Override
	public String getResourceUrl( TrueNTHOAuthConfig config, String path ) {

		return config.getResourceUrl( )
					 .concat( path );
	}

	@Override
	public String getRolesUrl( TrueNTHOAuthConfig config, long userId ) {

		return config.getRolesUrl( )
					 .replaceFirst( USER_ID, String.valueOf( userId ) );
	}

	@Override
	public void signRequest( TrueNTHOAuthConfig config, TrueNTHAccessToken accessToken, HttpRequestBase request ) {

		try {
			switch ( config.getSignatureType( ) ) {
				case BEARER_AUTHORIZATION_REQUEST_HEADER_FIELD:
					request.addHeader( OAUTH_HEADER_AUTHORIZATION, BEARER + " " + accessToken.getAccessToken( ) );
					break;
				case BEARER_URI_QUERY_PARAMETER:
					URIBuilder builder = new URIBuilder( request.getURI( ) );
					builder.setParameter( OAUTH_ACCESS_TOKEN, accessToken.getAccessToken( ) );
					request.setURI( builder.build( ) );
					break;
			}
		}
		catch ( URISyntaxException e ) {throw new RuntimeException( e );}
	}

}
