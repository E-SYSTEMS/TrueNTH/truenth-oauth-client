package edu.uw.cirg.truenth.oauth.signedrequest.factory;

import edu.uw.cirg.truenth.oauth.internal.signedrequest.TrueNTHSignedRequestImpl;
import edu.uw.cirg.truenth.oauth.signedrequest.TrueNTHSignedRequest;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public final class TrueNTHSignedRequestFactory {

	public static TrueNTHSignedRequest getTrueNTHSignedRequest( String signedRequest, String validationKey ) throws InvalidKeyException, NoSuchAlgorithmException {

		return new TrueNTHSignedRequestImpl( signedRequest, validationKey );
	}

}
