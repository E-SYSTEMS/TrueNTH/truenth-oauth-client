package edu.uw.cirg.truenth.oauth.client.internal.requests;

import edu.uw.cirg.truenth.oauth.client.TrueNTHOAuthClient;
import edu.uw.cirg.truenth.oauth.tokens.TrueNTHAccessToken;
import edu.uw.cirg.truenth.ss.internal.roles.SSRoleExtractorJson;
import edu.uw.cirg.truenth.ss.roles.SSRole;

import javax.json.JsonObject;
import java.io.IOException;
import java.util.List;

public final class TrueNTHRolesRequestUtil {

	private static final SSRoleExtractorJson roleExtractor = new SSRoleExtractorJson( );

	public static List< SSRole > executeRequest( TrueNTHOAuthClient trueNTHOAuthClient, long trueNTHUserId, TrueNTHAccessToken accessToken ) throws IOException {

		String address = trueNTHOAuthClient.getRolesUrl( trueNTHUserId );

		JsonObject data = TrueNTHJsonRequestUtil.executeRequest( trueNTHOAuthClient, address, accessToken );

		return roleExtractor.extractRoles( data );

	}

}

