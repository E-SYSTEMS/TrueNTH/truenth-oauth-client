package edu.uw.cirg.truenth.ss.roles.factory;

import edu.uw.cirg.truenth.ss.internal.roles.SSRoleImpl;
import edu.uw.cirg.truenth.ss.roles.SSRole;

public final class SSRoleFactory {

	public static SSRole getSSRole( ) {

		return new SSRoleImpl( );
	}
}
