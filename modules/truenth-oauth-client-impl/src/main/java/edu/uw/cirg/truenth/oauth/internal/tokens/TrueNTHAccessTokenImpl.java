package edu.uw.cirg.truenth.oauth.internal.tokens;

import edu.uw.cirg.truenth.oauth.tokens.TrueNTHAccessToken;
import edu.uw.cirg.truenth.oauth.tokens.TrueNTHRefreshToken;
import edu.uw.cirg.truenth.oauth.tokens.TrueNTHTokenType;

import java.time.Instant;

public class TrueNTHAccessTokenImpl implements TrueNTHAccessToken {

	private final int expiresIn;
	private final Instant date;
	private final TrueNTHRefreshToken refreshToken;
	private final TrueNTHTokenType tokenType;
	private final String accessToken;
	private final String scope;
	private static final long serialVersionUID = 329140790394969559L;

	public TrueNTHAccessTokenImpl( String accessToken, String refreshToken, Instant date, int expiresIn, String scope ) {

		this.date = date;

		tokenType = TrueNTHTokenType.BEARER;
		this.refreshToken = new TrueNTHRefreshTokenImpl( refreshToken );
		this.accessToken = accessToken;
		this.expiresIn = expiresIn;
		this.scope = scope;
	}

	@Override
	public int hashCode( ) {

		int prime = 31;
		int result = 1;
		result = ( prime * result ) + accessToken.hashCode( );
		result = ( prime * result ) + refreshToken.hashCode( );
		return result;
	}

	@Override
	public boolean equals( Object obj ) {

		if ( this == obj ) { return true; }
		if ( obj == null ) { return false; }
		if ( getClass( ) != obj.getClass( ) ) { return false; }
		TrueNTHAccessTokenImpl other = ( TrueNTHAccessTokenImpl ) obj;
		if ( accessToken == null ) {
			if ( other.accessToken != null ) { return false; }
		}
		else if ( !accessToken.equals( other.accessToken ) ) { return false; }
		return refreshToken.equals( other.refreshToken );
	}

	@Override
	public String getAccessToken( ) {

		return accessToken;
	}

	@Override
	public Instant getDate( ) {

		return date;
	}

	@Override
	public Integer getExpiresIn( ) {

		return expiresIn;
	}

	@Override
	public TrueNTHRefreshToken getRefreshToken( ) {

		return refreshToken;
	}

	@Override
	public String getScope( ) {

		return scope;
	}

	@Override
	public boolean isExpired( ) {

		return date.plusSeconds( expiresIn )
				   .isBefore( Instant.now( ) );
	}

	@Override
	public TrueNTHTokenType getTokenType( ) {

		return tokenType;
	}
}
