package edu.uw.cirg.truenth.oauth.client.internal.requests;

import edu.uw.cirg.truenth.oauth.client.TrueNTHOAuthClient;
import edu.uw.cirg.truenth.oauth.tokens.TrueNTHAccessToken;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.IOException;

public final class TrueNTHAccessTokenRequestUtil {

	public static TrueNTHAccessToken executeRequest( TrueNTHOAuthClient trueNTHOAuthClient, String code ) throws IOException {

		try (
				CloseableHttpClient httpClient = HttpClientBuilder.create( )
																  .build( )
		) {

			HttpPost httpPost = TrueNTHRequestFactory.getAccessTokenRequest( trueNTHOAuthClient, code );

			return httpClient.execute( httpPost, new TrueNTHAccessTokenResponseHandler( ) );
		}
	}

}
