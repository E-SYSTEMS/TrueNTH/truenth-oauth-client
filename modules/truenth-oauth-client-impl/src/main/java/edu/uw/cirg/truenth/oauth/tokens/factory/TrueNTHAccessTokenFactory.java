package edu.uw.cirg.truenth.oauth.tokens.factory;

import edu.uw.cirg.truenth.oauth.internal.tokens.TrueNTHAccessTokenImpl;
import edu.uw.cirg.truenth.oauth.internal.tokens.TrueNTHRefreshTokenImpl;
import edu.uw.cirg.truenth.oauth.tokens.TrueNTHAccessToken;
import edu.uw.cirg.truenth.oauth.tokens.TrueNTHRefreshToken;

import java.time.Instant;

public final class TrueNTHAccessTokenFactory {

	public static TrueNTHAccessToken getExpired( TrueNTHAccessToken accessToken ) {

		String tokenString = accessToken.getAccessToken( );
		String refreshToken = accessToken.getRefreshToken( )
										 .getToken( );
		Instant date = accessToken.getDate( );
		Integer expiresIn = 0;
		String scope = accessToken.getScope( );
		return new TrueNTHAccessTokenImpl( tokenString, refreshToken, date, expiresIn, scope );
	}

	public static TrueNTHAccessToken getTrueNTHAccessToken( String accessToken, String refreshToken, Instant date, Integer expiresIn, String scope ) {

		return new TrueNTHAccessTokenImpl( accessToken, refreshToken, date, expiresIn, scope );
	}

	public static TrueNTHRefreshToken getTrueNTHRefreshToken( String value ) {

		return new TrueNTHRefreshTokenImpl( value );
	}
}
