package edu.uw.cirg.truenth.oauth.internal.tokens;

import edu.uw.cirg.truenth.oauth.tokens.TrueNTHRefreshToken;
import edu.uw.cirg.truenth.oauth.tokens.TrueNTHTokenType;

public class TrueNTHRefreshTokenImpl implements TrueNTHRefreshToken {

	private final String token;
	private final TrueNTHTokenType tokenType;
	private static final long serialVersionUID = 4351396514937408267L;

	public TrueNTHRefreshTokenImpl( String token ) {

		this.token = token;
		tokenType = TrueNTHTokenType.BEARER;
	}

	@Override
	public int hashCode( ) {

		return token.hashCode( );
	}

	@Override
	public boolean equals( Object obj ) {

		if ( this == obj ) { return true; }
		if ( !( obj instanceof TrueNTHRefreshTokenImpl ) ) { return false; }

		TrueNTHRefreshTokenImpl other = ( TrueNTHRefreshTokenImpl ) obj;
		return token.equals( other.token );
	}

	@Override
	public String toString( ) {

		return getToken( );
	}

	@Override
	public String getToken( ) {

		return token;
	}

	@Override
	public TrueNTHTokenType getTokenType( ) {

		return tokenType;
	}

}
