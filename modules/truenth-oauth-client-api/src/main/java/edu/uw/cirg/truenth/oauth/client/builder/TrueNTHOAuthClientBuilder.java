package edu.uw.cirg.truenth.oauth.client.builder;

import edu.uw.cirg.truenth.oauth.client.TrueNTHOAuthClient;
import edu.uw.cirg.truenth.oauth.tokens.TrueNTHSignatureType;

public interface TrueNTHOAuthClientBuilder {

	TrueNTHOAuthClientBuilder accessTokenEndpointURL( String accessTokenEndpointURL );

	TrueNTHOAuthClientBuilder accessTokenStatusEndpointURL( String accessTokenStatusEndpointURL );

	TrueNTHOAuthClientBuilder apiKey( String apiKey );

	TrueNTHOAuthClientBuilder apiSecret( String apiSecret );

	TrueNTHOAuthClientBuilder baseAuthorizationURL( String baseAuthorizationURL );

	TrueNTHOAuthClientBuilder baseURL( String baseURL );

	TrueNTHOAuthClient build( );

	TrueNTHOAuthClientBuilder callbackURL( String callback );

	TrueNTHOAuthClientBuilder resourceURL( String resourceURL );

	TrueNTHOAuthClientBuilder rolesURL( String rolesURL );

	TrueNTHOAuthClientBuilder scope( String scope );

	String getAccessTokenEndpointURL( );

	String getAccessTokenStatusEndpointURL( );

	String getApiKey( );

	String getApiSecret( );

	String getBaseAuthorizationURL( );

	String getBaseURL( );

	String getCallbackURL( );

	String getResourceURL( );

	String getRolesURL( );

	String getScope( );

	TrueNTHSignatureType getSignatureType( );
}
