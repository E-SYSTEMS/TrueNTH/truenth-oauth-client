package edu.uw.cirg.truenth.oauth.tokens;

import java.io.Serializable;

public interface TrueNTHToken extends Serializable {

	TrueNTHTokenType getTokenType( );
}
