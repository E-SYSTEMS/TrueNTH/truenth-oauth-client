package edu.uw.cirg.truenth.oauth.tokens;

import java.time.Instant;

public interface TrueNTHAccessToken extends TrueNTHToken {

	String getAccessToken( );

	Instant getDate( );

	Integer getExpiresIn( );

	TrueNTHRefreshToken getRefreshToken( );

	String getScope( );

	boolean isExpired( );

}
