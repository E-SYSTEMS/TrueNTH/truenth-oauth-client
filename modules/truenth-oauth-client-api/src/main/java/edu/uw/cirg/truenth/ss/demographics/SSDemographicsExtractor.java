package edu.uw.cirg.truenth.ss.demographics;

import java.net.URL;
import java.util.Calendar;

/**
 * Generic demographic information extractor.
 *
 * @param <T> Data source object type.
 * @author Victor de Lima Soares
 * @since Mar 25, 2016
 */
public interface SSDemographicsExtractor < T > {

	Calendar extractBirthday( T data );

	SSDemographics extractDemographics( T data );

	String extractEmail( T data );

	String extractFirstName( T data );

	String extractGender( T data );

	String extractLastName( T data );

	URL extractPhotoUrl( T data );

	String extractTrueNTHId( T data );

	String extractTrueNTHUsername( T data );

}
