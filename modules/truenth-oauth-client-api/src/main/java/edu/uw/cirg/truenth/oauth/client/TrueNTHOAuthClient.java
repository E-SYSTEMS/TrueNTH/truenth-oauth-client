package edu.uw.cirg.truenth.oauth.client;

import edu.uw.cirg.truenth.oauth.tokens.TrueNTHAccessToken;
import edu.uw.cirg.truenth.ss.demographics.SSDemographics;
import edu.uw.cirg.truenth.ss.roles.SSRole;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpRequestBase;

import javax.json.JsonObject;
import java.io.IOException;
import java.util.List;

public interface TrueNTHOAuthClient {

	TrueNTHAccessToken getAccessToken( String code ) throws IOException;

	String getAccessTokenEndpoint( );

	TrueNTHAccessToken getAccessTokenStatus( TrueNTHAccessToken accessToken ) throws IOException;

	String getAuthorizationUrl( List< NameValuePair > callbackParameters, List< NameValuePair > parameters );

	String getAuthorizationUrl( List< NameValuePair > callbackParameters );

	String getResource( String path, TrueNTHAccessToken accessToken ) throws IOException;

	SSDemographics getResourceDemographics( TrueNTHAccessToken accessToken ) throws IOException;

	JsonObject getResourceJson( String path, TrueNTHAccessToken accessToken ) throws IOException;

	String getResourceDemographicsUrl( );

	String getResourceUrl( String path );

	String getRolesUrl( long userId );

	List< SSRole > getTrueNTHRoles( long trueNTHUserId, TrueNTHAccessToken accessToken ) throws IOException;

	boolean hasScope( );

	boolean isAccessTokenActive( TrueNTHAccessToken accessToken ) throws IOException;

	void signRequest( TrueNTHAccessToken accessToken, HttpRequestBase request );

	String getAccessTokenStatusEndpoint( );

	String getApiKey( );

	String getApiSecret( );

	String getAuthorizationUrl( );

	String getCallback( );

	String getScope( );

	TrueNTHOAuthConfig getTrueNTHOAuthConfig( );

	String getVersion( );
}
