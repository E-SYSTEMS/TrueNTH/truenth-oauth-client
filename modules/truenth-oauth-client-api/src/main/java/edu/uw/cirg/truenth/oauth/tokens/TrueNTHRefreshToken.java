package edu.uw.cirg.truenth.oauth.tokens;

public interface TrueNTHRefreshToken extends TrueNTHToken {

	String getToken( );
}
