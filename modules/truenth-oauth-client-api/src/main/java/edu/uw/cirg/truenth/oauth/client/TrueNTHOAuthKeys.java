package edu.uw.cirg.truenth.oauth.client;

public interface TrueNTHOAuthKeys {

	/**
	 * Error messages' identification, sent by SS.
	 */
	String ERROR = "error";

	/**
	 * Used to internally redirect users.
	 * <p>
	 * Used internally to redirect users after an OAuth call.
	 * </p>
	 * <p>
	 * This parameter is similar to <code>next</code>, as it is destined to
	 * perform redirections made by the local server that originated the OAuth
	 * calls. <code>next</code> was used the redirect clients coming from SS,
	 * after an OAuth login operation, but was removed from the protocol.
	 * </p>
	 * <p>
	 * On normal circumstances, this parameter should be preferred as it allows
	 * the server to control redirection with more flexibility. Additionally,
	 * redirection can occur faster, and more efficiently.
	 * </p>
	 * <p>
	 * This parameter should be embedded into the callback URLs.
	 * </p>
	 */
	String REDIRECT = "target";

	String SS_PATH_DEMOGRAPHICS = "/demographics";

	// OAuth
	String OAUTH_HEADER_AUTHORIZATION = "Authorization";
	String OAUTH_SCOPES = "scopes";
	String OAUTH_SCOPE = "scope";
	String OAUTH_EXPIRES_IN = "expires_in";
	String OAUTH_TOKEN_TYPE = "token_type";

	// OAuth
	String OAUTH_ACCESS_TOKEN = "access_token";
	String OAUTH_CLIENT_ID = "client_id";
	String OAUTH_CLIENT_SECRET = "client_secret";
	String OAUTH_REDIRECT_URI = "redirect_uri";
	String OAUTH_CODE = "code";
	String OAUTH_REFRESH_TOKEN = "refresh_token";
	String OAUTH_GRANT_TYPE = "grant_type";
	String OAUTH_AUTHORIZATION_CODE = "authorization_code";
	String OAUTH_RESPONSE_TYPE = "response_type";
	String OAUTH_RESPONSE_TYPE_CODE = "code";

}
