package edu.uw.cirg.truenth.ss.demographics;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;

public interface SSDemographics {

	Calendar getBirthday( );

	String getEmail( );

	String getFirstName( );

	boolean getGender( );

	String getLastName( );

	URL getPhotoUrl( );

	long getTrueNTHID( );

	String getTrueNTHUsername( );

	void setBirthday( Calendar birthday );

	void setEmail( String email );

	void setFirstName( String firstName );

	void setGender( boolean isMale );

	void setGender( String gender );

	void setLastName( String lastName );

	void setPhotoUrl( String url ) throws MalformedURLException;

	void setPhotoUrl( URL url );

	void setTrueNTHID( String trueNTHID );

	void setTrueNTHID( long trueNTHID );

	void setTrueNTHUsername( String trueNTHUsername );
}
