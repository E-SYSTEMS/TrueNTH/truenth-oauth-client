package edu.uw.cirg.truenth.oauth.client;

import edu.uw.cirg.truenth.oauth.tokens.TrueNTHSignatureType;

public interface TrueNTHOAuthConfig {

	boolean hasScope( );

	String getAccessTokenEndpoint( );

	String getAccessTokenStatusEndpoint( );

	String getApiKey( );

	String getApiSecret( );

	String getBaseAuthorizationUrl( );

	String getBaseUrl( );

	String getCallback( );

	String getResourceUrl( );

	String getRolesUrl( );

	String getScope( );

	TrueNTHSignatureType getSignatureType( );

}
