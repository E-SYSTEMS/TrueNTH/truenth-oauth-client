package edu.uw.cirg.truenth.oauth.client.builder;

import edu.uw.cirg.truenth.oauth.client.TrueNTHOAuthClient;
import edu.uw.cirg.truenth.oauth.client.TrueNTHOAuthConfig;
import edu.uw.cirg.truenth.oauth.tokens.TrueNTHAccessToken;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpRequestBase;

import java.util.List;

public interface TrueNTHOAuthProvider {

	TrueNTHOAuthClient createService( TrueNTHOAuthConfig config );

	String getAccessTokenEndpoint( TrueNTHOAuthConfig config );

	String getAccessTokenStatusEndpoint( TrueNTHOAuthConfig config );

	String getAuthorizationUrl( TrueNTHOAuthConfig config );

	String getAuthorizationUrl( TrueNTHOAuthConfig config, List< NameValuePair > callbackParameters, List< NameValuePair > parameters );

	String getCallback( TrueNTHOAuthConfig config );

	String getResourceUrl( TrueNTHOAuthConfig config, String path );

	String getRolesUrl( TrueNTHOAuthConfig config, long userId );

	void signRequest( TrueNTHOAuthConfig config, TrueNTHAccessToken accessToken, HttpRequestBase request );

}
