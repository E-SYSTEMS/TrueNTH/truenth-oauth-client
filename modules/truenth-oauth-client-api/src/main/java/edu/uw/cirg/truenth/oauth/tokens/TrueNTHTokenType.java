package edu.uw.cirg.truenth.oauth.tokens;

public enum TrueNTHTokenType {

	BEARER( "Bearer" );

	private final String tokenType;

	TrueNTHTokenType( String tokenType ) {

		this.tokenType = tokenType;
	}

	public static TrueNTHTokenType getByName( String tokenType ) {

		tokenType = tokenType.trim( )
							 .toUpperCase( );

		return valueOf( tokenType );

	}

	@Override
	public String toString( ) {

		return tokenType;
	}
}
