package edu.uw.cirg.truenth.ss.roles;

import java.util.List;

public interface SSRoleExtractor < T > {

	String extractDescription( T data );

	String extractName( T data );

	SSRole extractRole( T data );

	List< SSRole > extractRoles( T data );
}
