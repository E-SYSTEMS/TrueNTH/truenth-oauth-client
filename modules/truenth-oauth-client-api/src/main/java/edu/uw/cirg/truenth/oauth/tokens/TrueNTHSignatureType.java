package edu.uw.cirg.truenth.oauth.tokens;

public enum TrueNTHSignatureType {
	BEARER_AUTHORIZATION_REQUEST_HEADER_FIELD, BEARER_URI_QUERY_PARAMETER
}
