package edu.uw.cirg.truenth.oauth.signedrequest;

import edu.uw.cirg.truenth.oauth.tokens.TrueNTHRefreshToken;

import javax.json.JsonObject;

public interface TrueNTHSignedRequest {

	String USER_ID = "user_id";
	String ALGORITHM = "algorithm";
	String ISSUED_AT = "issued_at";
	String EVENT = "event";
	String REFRESH_TOKEN = "refresh_token";

	JsonObject getData( );

	String getSignature( );

	String getUserId( );

	String getAlgorithm( );

	String getIssuedAt( );

	String getEvent( );

	TrueNTHRefreshToken getRefreshToken( );

}
