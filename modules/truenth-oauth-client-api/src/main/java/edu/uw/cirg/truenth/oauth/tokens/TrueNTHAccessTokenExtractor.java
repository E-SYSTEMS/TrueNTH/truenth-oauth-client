package edu.uw.cirg.truenth.oauth.tokens;

import java.time.Instant;

public interface TrueNTHAccessTokenExtractor < T > {

	TrueNTHAccessToken extract( String data, Instant date );

	TrueNTHAccessToken extract( T data, Instant date );

}
