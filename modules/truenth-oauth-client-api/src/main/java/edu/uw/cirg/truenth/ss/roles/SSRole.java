package edu.uw.cirg.truenth.ss.roles;

import java.io.Serializable;

public interface SSRole extends Serializable {

	String getDescription( );

	String getName( );

	void setDescription( String ssDescription );

	void setName( String ssName );
}
