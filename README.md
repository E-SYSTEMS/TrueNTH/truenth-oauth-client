# JTrueNTHOAuth-Client

TrueNTHConnect includes its own OAuth library to make communication
with Shared Services easier and based on simple calls. These components were designed to provide a stable
and uniform methodology for issuing requests and interpreting their outcomes, while observing our specific OAuth needs.

Additionally, the library was conceived as an independent
project, not linked in anyway to the portal, in order to make it available to
other projects to come. 

This project was renamed and modularized as truenth-oauth-client

<pre>
Root project 'truenth-oauth-client'
\--- Project ':modules'
     +--- Project ':modules:truenth-oauth-client-api' - TrueNTH OAuth client - API
     +--- Project ':modules:truenth-oauth-client-impl' - TrueNTH OAuth client - impl
     +--- Project ':modules:truenth-oauth-client-osgi-api' - TrueNTH OAuth client - OSGi API
     \--- Project ':modules:truenth-oauth-client-osgi-impl' - TrueNTH OAuth client - OSGi impl
</pre>

OSGi jars are not required, but are made available for OSGi users.

[Original project](https://github.com/uwcirg/JTrueNTHOAuth-Client)

## Maven

```xml
<distributionManagement>
    <repository>
        <id>central</id>
        <name>Artifactory-releases</name>
        <url>https://builder.e-systems.tech/artifactory/libs-release-pub</url>
    </repository>
</distributionManagement>
```

---

```xml
<dependency>
    <groupId>edu.uw.cirg.truenth.oauth</groupId>
    <artifactId>truenth-oauth-client-api</artifactId>
    <version>1.0.0</version>
</dependency>
```
 
<pre>
Dependencies compile:
+--- javax.json:javax.json-api
\--- org.apache.httpcomponents:httpclient
     +--- org.apache.httpcomponents:httpcore
     +--- commons-logging:commons-logging
     \--- commons-codec:commons-codec
</pre>

---

```xml
<dependency>
    <groupId>edu.uw.cirg.truenth.oauth</groupId>
    <artifactId>truenth-oauth-client-impl</artifactId>
    <version>1.0.0</version>
</dependency>
```

<pre>
Dependencies compile:
+--- org.apache.httpcomponents:httpclient
|    +--- org.apache.httpcomponents:httpcore
|    +--- commons-logging:commons-logging
|    \--- commons-codec:commons-codec
+--- org.apache.httpcomponents:httpmime
|    \--- org.apache.httpcomponents:httpclient
\--- project :modules:truenth-oauth-client-api
</pre>

---

## How to use

### 1. Create a service

A client is the main object that will provide all OAuth services and centralize all necessary configuration. 

The service class is named TrueNTHOAuthClient
and the following example demonstrates how to create an instance using the staging server configuration to illustrate possible values.

```Java
TrueNTHOAuthClient client = TrueNTHOAuthClientBuilderFactory.getTrueNTHOAuthClientBuilder( )
										.baseAuthorizationURL("https://stg.us.truenth.org/oauth/authorize")
										.accessTokenEndpointURL("https://stg.us.truenth.org/oauth/token")
										.accessTokenStatusEndpointURL("https://stg.us.truenth.org/oauth/oauth/token-status")
										.baseURL("https://stg.us.truenth.org")
										.resourceURL("https://stg.us.truenth.org/api")
										.rolesURL(https://stg.us.truenth.org/api/user/#userId/roles)
										.callbackURL(YOUR_APP_CALLBACK_URL) 	// This is where users land after getting an Authorization code from SS.
										.apiKey(YOUR_API_KEY)
										.apiSecret(YOUR_API_SECRET)
										.build();
```

[SS API specification](https://stg.us.truenth.org/dist/)

This is what this client can do for you:

```Java
public interface TrueNTHOAuthClient {

	TrueNTHAccessToken getAccessToken( String code ) throws IOException;

	String getAccessTokenEndpoint( );

	TrueNTHAccessToken getAccessTokenStatus( TrueNTHAccessToken accessToken ) throws IOException;

	String getAuthorizationUrl( List< NameValuePair > callbackParameters, List< NameValuePair > parameters );

	String getAuthorizationUrl( List< NameValuePair > callbackParameters );

	String getResource( String path, TrueNTHAccessToken accessToken ) throws IOException;

	SSDemographics getResourceDemographics( TrueNTHAccessToken accessToken ) throws IOException;

	JsonObject getResourceJson( String path, TrueNTHAccessToken accessToken ) throws IOException;

	String getResourceDemographicsUrl( );

	String getResourceUrl( String path );

	String getRolesUrl( long userId );

	List< SSRole > getTrueNTHRoles( long trueNTHUserId, TrueNTHAccessToken accessToken ) throws IOException;

	boolean hasScope( );

	boolean isAccessTokenActive( TrueNTHAccessToken accessToken ) throws IOException;

	void signRequest( TrueNTHAccessToken accessToken, HttpRequestBase request );

	String getAccessTokenStatusEndpoint( );

	String getApiKey( );

	String getApiSecret( );

	String getAuthorizationUrl( );

	String getCallback( );

	String getScope( );

	TrueNTHOAuthConfig getTrueNTHOAuthConfig( );

	String getVersion( );
}
```
